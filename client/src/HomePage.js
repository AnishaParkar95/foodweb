import React from 'react';
import NavBar from './Components/NavBar';
import Carousel from './Components/Carousel'
import Axios from 'axios';
import RecipeCardsHome from './Components/RecipeCardsHome';
// import { Link } from 'react-router-dom';

class HomePage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      recipes: [],
    }
    this.loadRecipes = this.loadRecipes.bind(this);
  }


  async componentDidMount() {
    await this.loadRecipes()
  }

  async loadRecipes() {
    try {
      console.log("in home loadrecipes try start")
      const response = await Axios.get('/api/addrecipe');
      console.log("render home:", JSON.stringify(response.data))
      const { data } = response;
      this.setState({ recipes: data });
      console.log("in home loadrecipes try end")

    } catch (error) {
      console.log("in home loadrecipes error")
      console.error(error.message);

    }
  }
  render() {
    const { recipes } = this.state
    console.log("render me master:", JSON.stringify(recipes))
    const cards = recipes;

    return (
      <React.Fragment>
        <NavBar />
        <div style={{ marginTop: '4.5rem' }}>
          <Carousel />
        </div>
        {/* <Link to="/recipedetails" style={{ textDecoration: 'none' }}><RecipeCardsHome cards={cards} /></Link> */}
        <RecipeCardsHome cards={cards} />
        {/* <Link to="/recipedetails" style={{ textDecoration: 'none' }}><RecipeCardsHome cards={cards} /></Link> */}
      </React.Fragment>
    )
  }
}
export default HomePage

