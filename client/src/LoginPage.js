import React from "react";
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import EmailAddressInput from './Components/EmailAddressInput'
import PasswordInput from "./Components/PasswordInput";
import './style.css';
import { withRouter } from 'react-router-dom';
import NavBar from "./Components/NavBar";
import LoginContext from './LoginContext'

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

            userEmailAddress: '',
            userPassword: ''
        };

        this.handleUserEmailAddressChange = this.handleUserEmailAddressChange.bind(this);
        this.handleUserPasswordChange = this.handleUserPasswordChange.bind(this);
        this.handleUserSubmit = this.handleUserSubmit.bind(this);
    }

    handleUserEmailAddressChange(event) {
        this.setState({ userEmailAddress: event.target.value });
    }

    handleUserPasswordChange(event) {
        this.setState({ userPassword: event.target.value });
    }

    async handleUserSubmit() {

        const { LoggedStatus } = this.context;
        console.log("hey this is where ia m accessing context", LoggedStatus)
        const { history } = this.props;
        const { userEmailAddress, userPassword } = this.state;

        try {

            // This is the JSON payload that will be delivered to the server in 'request.body'
            const data = { emailAddress: userEmailAddress, password: userPassword };

            // We are now doing a POST request because we are storing a new user
            await Axios.post('/api/login', data);
            // LoggedStatus();
            history.push('/home');


        } catch (error) {

            console.error(error.message);
        }

        // Reload the users straight from the server
        await this.loadUsers();
    }

    // Context code
    static contextType = LoginContext;


    render() {

        const { userEmailAddress, userPassword } = this.state;
        return (
            <div className='loginPageBackground'>
                <NavBar />
                <Container component="main" maxWidth="xs" style={{ marginTop: "0rem" }}>
                    <CssBaseline />
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
                        <Avatar style={{ backgroundColor: 'black', marginTop: "21rem" }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                </Typography>
                        <form style={{ width: '100%', marginTop: "1rem" }} noValidate>
                            <EmailAddressInput emailaddress={userEmailAddress} onEmailAddressChange={this.handleUserEmailAddressChange} />
                            <PasswordInput password={userPassword} onPasswordChange={this.handleUserPasswordChange} />
                            <Button type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={this.handleUserSubmit}
                                style={{ backgroundColor: 'black', marginTop: '1rem' }}>
                                Sign In
                </Button>
                            <Grid container style={{ marginTop: '1rem' }}>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Forgot password?
                        </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="/registration" variant="body2">
                                        {"Don't have an account? Sign Up"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Container>
            </div>
        );
    }
}

export default withRouter(Login);