import React from 'react'
import NavBar from './Components/NavBar'
import { withRouter } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';
import Container from '@material-ui/core/Container'
import Icon from '@material-ui/core/Icon';
import Rating from '@material-ui/lab/Rating';

const styles = theme => ({
  root: {
    '& > span': {
      margin: theme.spacing(2),
    },
  },

  mainFeaturedPost: {
    position: 'relative',
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    marginBottom: theme.spacing(4),
    backgroundImage: 'url(https://airfryer.cooking/wp-content/uploads/2017/12/Chicken_Burger_patty_large-1024x647.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  mainFeaturedPostContent: {
    position: 'relative',
    padding: theme.spacing(3),
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
  mainGrid: {
    marginTop: theme.spacing(3),
  },
  markdown: {
    ...theme.typography.body2,
    padding: theme.spacing(3, 0),
  },
  sidebarAboutBox: {
    padding: theme.spacing(2),
    backgroundColor: theme.palette.grey[200],
  },
  sidebarSection: {
    marginTop: theme.spacing(3),
  },

});

const archives = [
  '525 calories',
  '39.9 g total fat',
  '179 mg cholesterol',
  '915 mg sodium',
  '10.2 g carbohydrates',
  '31.7 g protein',
];

const social = ['Prep : 15 min', 'Cook : 15 min', 'Servings : 6'];

class RecipeDetails extends React.Component {

  render() {
    const { classes } = this.props
    return (
      <div style={{ marginTop: '5rem' }}>

        <React.Fragment>
          <NavBar />
          <CssBaseline />
          <Container maxWidth="lg">
            <main>
              {/* Main featured post */}
              <Paper className={classes.mainFeaturedPost}>
                {/* Increase the priority of the hero background image */}
                {
                  <img
                    style={{ display: 'none' }}
                    src="https://airfryer.cooking/wp-content/uploads/2017/12/Chicken_Burger_patty_large-1024x647.jpg"
                    alt="background"
                  />
                }
                <div className={classes.overlay} />
                <Grid container>
                  <Grid item md={6}>
                    <div className={classes.mainFeaturedPostContent}>
                      <Typography component="h1" variant="h3" color="inherit" gutterBottom>
                        Chicken Burger
                      </Typography>
                      <Typography variant="h5" color="inherit" paragraph>
                        Recipe by: Anisha Parkar
                    </Typography>
                      <Rating name="half-rating" value={3.5} precision={0.5} />
                      <h4>469 Ratings | 298 Reviews | 20 Photos</h4>
                    </div>
                  </Grid>
                </Grid>
              </Paper>
              {/* End main featured post */}
              <Grid container spacing={5} className={classes.mainGrid}>
                {/* Main content */}
                <Grid item xs={12} md={8}>
                  <Typography variant="h5" gutterBottom style={{ marginTop: '1rem' }}>
                    Ingredients
                  </Typography>
                  <Divider />
                  <Typography>
                    <div style={{ display: 'flex' }}>
                      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                      <Icon>add_circle</Icon>
                      Chicken
                     </div>
                  </Typography>
                  <Typography variant="h5" gutterBottom style={{ marginTop: '1rem' }}>
                    Instructions
                  </Typography>
                  <Divider />
                  <Typography>
                    <div style={{ display: 'flex' }}>
                      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                      <Icon>add_circle</Icon>
                      Chicken
                     </div>
                  </Typography>
                </Grid>
                {/* End main content */}
                {/* Sidebar */}
                <Grid item xs={12} md={4}>
                  <Paper elevation={0} className={classes.sidebarAboutBox}>
                    <Typography variant="h6" gutterBottom>
                      About
                </Typography>
                    <Typography>
                      This is a simple and great salad as a cooling accompaniment to curry or any spicy meal
                </Typography>
                  </Paper>
                  <br></br>
                  <Paper elevation={0} className={classes.sidebarAboutBox}>
                    <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
                      Information
                    </Typography>
                    {social.map(network => (
                      <Link display="block" variant="body1" href="#" key={network}>
                        {network}
                      </Link>
                    ))}
                  </Paper>
                  <br></br>
                  <Paper elevation={0} className={classes.sidebarAboutBox}>
                    <Typography variant="h6" gutterBottom className={classes.sidebarSection}>
                      Nutrition
              </Typography>
                    {archives.map(archive => (
                      <Link display="block" variant="body1" href="#" key={archive}>
                        {archive}
                      </Link>
                    ))}
                  </Paper>

                </Grid>
                {/* End sidebar */}
              </Grid>
            </main>
          </Container>
        </React.Fragment>
      </div>
    )
  }
}
export default withStyles(styles)(withRouter(RecipeDetails));