import React from "react";
import Axios from 'axios';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import EmailAddressInput from './Components/EmailAddressInput'
import PasswordInput from "./Components/PasswordInput";
import './style.css'
import { withRouter } from 'react-router-dom'
import FirstNameInput from "./Components/FirstNameInput";
import LastNameInput from "./Components/LastNameInput";


class RegistrationPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {

            users: [],

            userFName: '',
            userLName: '',
            userEmailAddress: '',
            userPassword: ''
        };

        this.loadUsers = this.loadUsers.bind(this);

        this.handleUserFNameChange = this.handleUserFNameChange.bind(this);
        this.handleUserLNameChange = this.handleUserLNameChange.bind(this);
        this.handleUserEmailAddressChange = this.handleUserEmailAddressChange.bind(this);
        this.handleUserPasswordChange = this.handleUserPasswordChange.bind(this);

        this.handleUserSubmit = this.handleUserSubmit.bind(this);
    }

    async componentDidMount() {

        await this.loadUsers()
    }

    async loadUsers() {

        try {

            const response = await Axios.get('/api/Users');

            const { data } = response;

            this.setState({ users: data });

        } catch (error) {

            console.error(error.message);
        }
    }

    handleUserFNameChange(event) {
        this.setState({ userFName: event.target.value });
    }

    handleUserLNameChange(event) {
        this.setState({ userLName: event.target.value });
    }

    handleUserEmailAddressChange(event) {
        this.setState({ userEmailAddress: event.target.value });
    }

    handleUserPasswordChange(event) {
        this.setState({ userPassword: event.target.value });
    }

    async handleUserSubmit() {

        const { userFName, userLName, userEmailAddress, userPassword } = this.state;

        try {

            // This is the JSON payload that will be delivered to the server in 'request.body'
            const data = { name: userFName, lname: userLName, emailAddress: userEmailAddress, password: userPassword };

            // We are now doing a POST request because we are storing a new user
            await Axios.post('/api/users', data);

        } catch (error) {

            console.error(error.message);
        }

        // Reload the users straight from the server
        await this.loadUsers();
    }

    render() {
        const { users, userFName, userLName, userEmailAddress, userPassword } = this.state;

        const userCards = users.map((user) => {

            return (
                <Card style={{ margin: '1rem' }} key={JSON.stringify(user)}>
                    <CardContent>
                        <Typography color={"textSecondary"}>
                            {user.name}
                            {user.lname}
                        </Typography>

                        <Typography color={"textSecondary"}>
                            {user.emailAddress}
                            {user.password}

                        </Typography>
                    </CardContent>
                </Card>
            )
        });

        return (
            <div className='loginPageBackground'>
                <Container component="main" maxWidth="xs" style={{ marginTop: "0rem" }}>
                    <CssBaseline />
                    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
                        <Avatar style={{ backgroundColor: 'black', marginTop: "21rem" }}>
                            <LockOutlinedIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign Up
                </Typography>
                        <form style={{ width: '100%', marginTop: "1rem" }} noValidate>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <FirstNameInput FirstName={userFName} onFirstNameChange={this.handleUserFNameChange} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <LastNameInput LastName={userLName} onLastNameChange={this.handleUserLNameChange} />
                                </Grid>
                            </Grid>
                            <EmailAddressInput emailaddress={userEmailAddress} onEmailAddressChange={this.handleUserEmailAddressChange} />
                            <PasswordInput password={userPassword} onPasswordChange={this.handleUserPasswordChange} />
                            <Button type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                style={{ backgroundColor: 'black', marginTop: '1rem' }}
                                onClick={this.handleUserSubmit} >
                                Sign Up
                    </Button>
                            <Grid container style={{ marginTop: '0.7rem' }}>
                                <Grid item xs style={{ marginLeft: '5rem' }}>
                                    <Link href="/" variant="body2">
                                        {"Already have an account? Sign In"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Container>
                {userCards}
            </div>
        );
    }
}

export default withRouter(RegistrationPage);