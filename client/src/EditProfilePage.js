import React from 'react';
import Axios from 'axios';
import NavBar from './Components/NavBar';
import FirstNameInput from './Components/FirstNameInput';
import LastNameInput from './Components/LastNameInput';
import EmailAddressInput from './Components/EmailAddressInput';
import PasswordInput from './Components/PasswordInput';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({

    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(15),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
});

class EditProfilePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {

            users: [],

            userFName: '',
            userLName: '',
            userEmailAddress: '',
            userPassword: ''
        };

        this.loadUsers = this.loadUsers.bind(this);

        this.handleUserFNameChange = this.handleUserFNameChange.bind(this);
        this.handleUserLNameChange = this.handleUserLNameChange.bind(this);
        this.handleUserEmailAddressChange = this.handleUserEmailAddressChange.bind(this);
        this.handleUserPasswordChange = this.handleUserPasswordChange.bind(this);

        this.handleUserUpdate = this.handleUserUpdate.bind(this);
    }


    async componentDidMount() {

        await this.loadUsers()
    }

    async loadUsers() {

        try {

            const response = await Axios.get('/api/Users');

            const { data } = response;

            this.setState({ users: data });

        } catch (error) {

            console.error(error.message);
        }
    }

    handleUserFNameChange(event) {
        this.setState({ userFName: event.target.value });
    }

    handleUserLNameChange(event) {
        this.setState({ userLName: event.target.value });
    }

    handleUserEmailAddressChange(event) {
        this.setState({ userEmailAddress: event.target.value });
    }

    handleUserPasswordChange(event) {
        this.setState({ userPassword: event.target.value });
    }

    async handleUserUpdate() {

        const { userFName, userLName, userEmailAddress, userPassword } = this.state;

        try {

            // This is the JSON payload that will be delivered to the server in 'request.body'
            const data = { name: userFName, lname: userLName, emailAddress: userEmailAddress, password: userPassword };

            // We are now doing a POST request because we are storing a new user
            await Axios.put('/api/users/_id', data);

        } catch (error) {

            console.error(error.message);
        }

        // Reload the users straight from the server
        await this.loadUsers();
    }

    render() {
        const { classes } = this.props
        const { userFName, userLName, userEmailAddress, userPassword } = this.state;

        return (
            <div>
                <React.Fragment>
                    <NavBar />
                    <main className={classes.layout}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center">
                                Edit Profile
                            </Typography>
                            <Typography variant="h6" gutterBottom>
                                Edit Profile Details
                            </Typography>
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={6}>
                                    <FirstNameInput FirstName={userFName} onFirstNameChange={this.handleUserFNameChange} />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <LastNameInput LastName={userLName} onLastNameChange={this.handleUserLNameChange} />
                                </Grid>
                                <Grid item xs={12}>
                                    <EmailAddressInput emailaddress={userEmailAddress} onEmailAddressChange={this.handleUserEmailAddressChange} />
                                </Grid>
                                <Grid item xs={12}>
                                    <PasswordInput password={userPassword} onPasswordChange={this.handleUserPasswordChange} />
                                </Grid>
                                <Button type="submit"
                                    variant="contained"
                                    color="primary"
                                    style={{ backgroundColor: 'black', marginTop: '1rem', alignContent: 'center' }}
                                    onClick={this.handleUserUpdate} >
                                    Update
                                </Button>
                            </Grid>
                        </Paper>
                    </main>
                </React.Fragment>
            </div>
        )
    }
}

export default withStyles(styles)(EditProfilePage);
