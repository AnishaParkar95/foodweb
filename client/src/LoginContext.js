import React, { createContext, Component } from 'react';

export const LoginContext = createContext();

class LoginContextProvider extends Component {

    state = { loginStatus: false }

    LoggedStatus = () => {
        this.setState({ loginStatus: !this.state.loginStatus })
    }

    render() {
        return (
            <LoginContext.Provider value={{ ...this.state, LoggedStatus: this.LoggedStatus }}>
                {this.props.children}
            </LoginContext.Provider>
        )
    }
}

export default LoginContextProvider
