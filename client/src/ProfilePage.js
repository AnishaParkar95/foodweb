import React from 'react';
import { withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import NavBar from './Components/NavBar';
import Axios from 'axios';
import { Link } from 'react-router-dom';
import RecipeCardsHome from './Components/RecipeCardsHome';

class ProfilePage extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            recipes: [],
        }
        this.loadRecipes = this.loadRecipes.bind(this);
    }

    async componentDidMount() {
        await this.loadRecipes()
    }

    async loadRecipes() {
        try {
            console.log("in home loadrecipes try start")
            const response = await Axios.get('/api/addrecipe');
            console.log("render home:", JSON.stringify(response.data))
            const { data } = response;
            this.setState({ recipes: data });
            console.log("in home loadrecipes try end")

        } catch (error) {
            console.log("in home loadrecipes error")
            console.error(error.message);

        }
    }

    render() {
        const { recipes } = this.state
        const cards = recipes;
        return (
            <div >
                <NavBar />
                <div style={{ position: 'relative', overflow: 'hidden', width: '89rem', height: '17rem', marginTop: '4.3rem' }}>
                    <img src="https://smppharmacy.com/wp-content/uploads/2019/02/food-post.jpg" alt="" />
                </div>
                <Grid container justify="center" alignItems="center" style={{ marginTop: '-9rem', position: 'absolute' }}>
                    <Avatar alt="Remy Sharp" src="https://cdn2.iconfinder.com/data/icons/circle-avatars-1/128/039_girl_avatar_profile_woman_headband-512.png" style={{ width: 210, height: 210, }} />
                    <h1 style={{ marginTop: '8rem', position: 'absolute' }}> Anisha Parkar</h1>
                    <Link to="/editprofile"><h4 style={{ marginTop: '11rem', position: 'absolute', marginLeft: '-10rem' }}> Edit Profile</h4></Link>
                    <Link to="/editprofile"><h4 style={{ marginTop: '13rem', position: 'absolute', marginLeft: '-11rem' }}> Favourite Recipes</h4></Link>
                </Grid>
                <div style={{ marginTop: '8rem' }}>
                    <RecipeCardsHome cards={cards} />
                </div>
            </div>

        )
    }
}
export default withRouter(ProfilePage);