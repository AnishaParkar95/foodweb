import React from 'react';
import NavBar from './Components/NavBar';
import { withRouter } from 'react-router-dom'
import Axios from 'axios';
import Typography from '@material-ui/core/Typography';
import { TextField, Divider, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import './style.css'

const styles = theme => ({

    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(10),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(8),
            marginBottom: theme.spacing(4),
            padding: theme.spacing(3),
        },
    },
});

class AddRecipe extends React.Component {

    constructor(props) {
        super(props)

        this.state = {

            recipeTitle: '',
            Description: '',
            Ingredients: '',
            Instructions: ''
        };

        this.handleRecipeTitleChange = this.handleRecipeTitleChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleIngredientsChange = this.handleIngredientsChange.bind(this);
        this.handleInstructionsChange = this.handleInstructionsChange.bind(this);
        this.handleRecipeSubmit = this.handleRecipeSubmit.bind(this);
    }

    handleRecipeTitleChange(event) {
        this.setState({ recipeTitle: event.target.value });
    }

    handleDescriptionChange(event) {
        this.setState({ Description: event.target.value });
    }

    handleIngredientsChange(event) {
        this.setState({ Ingredients: event.target.value });
    }

    handleInstructionsChange(event) {
        this.setState({ Instructions: event.target.value });
    }

    async handleRecipeSubmit() {
        const { recipeTitle, Description, Ingredients, Instructions } = this.state;
        try {
            const recipedata = { recipetitle: recipeTitle, description: Description, ingredients: Ingredients, instructions: Instructions }

            await Axios.post('/api/addrecipe', recipedata)

        } catch (error) {
            console.error(error.message);
        }
    }

    render() {
        const { recipeTitle, Description, Ingredients, Instructions } = this.state;
        const { classes } = this.props;

        return (
            <div className="recipepagebackground">
                <React.Fragment>
                    <NavBar />
                    <main className={classes.layout} style={{ marginTop: "5rem" }}>
                        <Paper className={classes.paper}>
                            <Typography component="h1" variant="h4" align="center" style={{ marginTop: '1.5rem' }}>
                                Add Recipe
                            </Typography>
                            <Divider />
                            <Typography component="h4" style={{ marginTop: '1.5rem' }}>Recipe title</Typography>
                            <TextField id="recipetitle"
                                fullWidth
                                defaultValue="Default Value"
                                margin="normal"
                                variant="outlined"
                                placeholder="Recipe Title"
                                value={recipeTitle}
                                onChange={this.handleRecipeTitleChange}
                            />
                            <Typography component="h4">Description</Typography>
                            <TextField id="outlined-multiline-static"
                                multiline
                                fullWidth
                                defaultValue="Default Value"
                                margin="normal"
                                variant="outlined"
                                placeholder="About the recipe"
                                value={Description}
                                onChange={this.handleDescriptionChange}
                            />
                            <Typography component="h4">Ingredients</Typography>
                            <TextField id="outlined-multiline-static"
                                multiline
                                fullWidth
                                defaultValue="Default Value"
                                margin="normal"
                                variant="outlined"
                                placeholder="Ingredients to cook"
                                value={Ingredients}
                                onChange={this.handleIngredientsChange}
                            />
                            <Typography component="h4">Instructions</Typography>
                            <TextField id="outlined-multiline-static"
                                multiline
                                fullWidth
                                defaultValue="Default Value"
                                margin="normal"
                                variant="outlined"
                                placeholder="Instructions for cooking"
                                value={Instructions}
                                onChange={this.handleInstructionsChange}
                            />
                            <Grid align="center" item>
                                <Button type="submit"
                                    variant="contained"
                                    color="primary"
                                    style={{ backgroundColor: 'black', marginTop: '1rem', }}
                                    onClick={this.handleRecipeSubmit} >
                                    Add Recipe
                                </Button>
                            </Grid>
                        </Paper>
                    </main>
                </React.Fragment>
            </div>
        );
    }
}
export default withStyles(styles)(withRouter(AddRecipe));