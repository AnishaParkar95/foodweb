import React from 'react';
import LoginPage from './LoginPage'
import RegistrationPage from './RegistrationPage'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import HomePage from './HomePage'
import AddRecipe from './AddRecipe';
import RecipeDetails from './RecipeDetails';
import ProfilePage from './ProfilePage';
import EditProfilePage from './EditProfilePage';
import LoginContextProvider from './LoginContext';

class App extends React.Component {
  render() {
    return (
      <LoginContextProvider>
        <div >
          <Router>
            <div>
              <Switch>
                <Route exact path="/">
                  <HomePage />
                </Route>
                <Route exact path="/loginpage">
                  <LoginPage />
                </Route>
                <Route exact path="/registration">
                  <RegistrationPage />
                </Route>
                <Route exact path="/addrecipepage">
                  <AddRecipe />
                </Route>
                <Route exact path="/home">
                  <HomePage />
                </Route>
                <Route exact path="/recipedetails">
                  <RecipeDetails />
                </Route>
                <Route exact path="/profilepage">
                  <ProfilePage />
                </Route>
                <Route exact path="/editprofile">
                  <EditProfilePage />
                </Route>
              </Switch>
            </div>
          </Router>

        </div>
      </LoginContextProvider>
    );
  }
}

export default App;