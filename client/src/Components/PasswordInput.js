import React from  'react';
import {TextField } from '@material-ui/core'

class PasswordInput extends React.Component {

    render() {

        const {password, onPasswordChange } = this.props;

        // const PasswordIsValid = password.length>=5;

        //  const helperText = PasswordIsValid ? '' : 'Password should be atleast 5 digits';

        return <TextField   variant="outlined"
                            margin={"normal"}
                            required
                            fullWidth 
                            label="Password" 
                            type="password" 
                            value={password} 
                            onChange={onPasswordChange}
                            //  error={!PasswordIsValid}
                            //  helperText={helperText}
                            />

    }

}

export default PasswordInput;