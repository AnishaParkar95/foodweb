import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { withRouter } from 'react-router-dom'

class NavBar extends React.Component{

    constructor(props){
        super(props)

        this.handleLogin = this.handleLogin.bind(this);
        this.handleAddRecipe = this.handleAddRecipe.bind(this);
        this.handleHome= this.handleHome.bind(this);
        this.handleProfile = this.handleProfile.bind(this);
    }

    handleLogin(event) {
        event.preventDefault();
        const { history } = this.props;
        history.push('/loginpage')
      };

    handleAddRecipe(event) {
        event.preventDefault();
        const { history } = this.props;
        history.push('/addrecipepage')
    };

    handleHome(event) {
            event.preventDefault();
            const { history } = this.props;
            history.push('/home')
      };
    
    handleProfile(event){
            event.preventDefault();
            const {history} = this.props;
            history.push('/profilepage')
    };

    render(){
        return(
    <div>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">
            Food Recipe
          </Typography>
          <Button color="inherit"
                    style={{marginLeft:'50rem'}}
                    onClick={this.handleHome}>
                    Home
            </Button>
            <Button color="inherit"
                    style={{marginLeft:'1rem'}}
                    onClick={this.handleAddRecipe}>
                    AddRecipe
            </Button>
            <Button color="inherit"
                    style={{marginLeft:'1rem'}}
                    onClick={this.handleProfile}>
                    Profile
            </Button>
          <Button color="inherit"
                    style={{marginLeft:'1rem'}}
                    onClick={this.handleLogin}>
                    Login
            </Button>
            
        </Toolbar>
      </AppBar>
    </div>
        );  
    }
}
export default withRouter(NavBar);

// import React from 'react';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
// import Typography from '@material-ui/core/Typography';
// import useScrollTrigger from '@material-ui/core/useScrollTrigger';
// import PropTypes from 'prop-types';
// import CssBaseline from '@material-ui/core/CssBaseline';
// import Button from '@material-ui/core/Button';
// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';

// class NavBar extends React.Component{

//     constructor(props){
//         super(props)
//     }
//     render(){

//         function ElevationScroll(props) {
//             const { children, window } = props;
//             // Note that you normally won't need to set the window ref as useScrollTrigger
//             // will default to window.
//             // This is only being set here because the demo is in an iframe.
//             const trigger = useScrollTrigger({
//               disableHysteresis: true,
//               threshold: 0,
//               target: window ? window() : undefined,
//             });
          
//             return React.cloneElement(children, {
//               elevation: trigger ? 4 : 0,
//             });
//           }
          
//           ElevationScroll.propTypes = {
//             children: PropTypes.element.isRequired,
//             /**
//              * Injected by the documentation to work in an iframe.
//              * You won't need it on your project.
//              */
//             window: PropTypes.func,
//           };

//         return(
//             <React.Fragment>
//             <CssBaseline />
//             <ElevationScroll >
//               <AppBar>
//                 <Toolbar>
//                   <Typography variant="h6">Scroll to Elevate App Bar</Typography>
//                 </Toolbar>
//               </AppBar>
//             </ElevationScroll>
//             <Toolbar />
//             </React.Fragment>
//         );

        
//     }

// }
// export default NavBar;