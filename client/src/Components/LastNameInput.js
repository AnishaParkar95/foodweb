import React from 'react';
import TextField from '@material-ui/core/TextField';

class LastNameInput extends React.Component {
    render() {
        const { LastName, onLastNameChange } = this.props
        return (
            <div>
                <TextField variant="outlined"
                    required
                    fullWidth
                    value={LastName}
                    onChange={onLastNameChange}
                    id="lastName"
                    label="Last Name"
                    name="lastName"
                    autoComplete="lname"
                />
            </div>
        )

    }
}
export default LastNameInput 
