import React from 'react';
import TextField from '@material-ui/core/TextField';

class EmailAddressInput extends React.Component {

    render() {

        const { emailaddress, onEmailAddressChange } = this.props;

        // const emailIsValid = emailaddress.includes('@');

        //  const helperText = emailIsValid ? '': 'Email should contain @ sign';
         
        return (
                <div>
                    <TextField   variant="outlined"
                            margin={"normal"}
                            required
                            fullWidth
                            label={"Email address" }
                            emailaddress={emailaddress} 
                            onChange={onEmailAddressChange}
                            //error={!emailIsValid}
                            // helperText={helperText} 
                            autoFocus 
                    />
                </div>
        )
    }
}
export default EmailAddressInput;