// import React from 'react';
// import makeCarousel from 'react-reveal/makeCarousel'
// import Slide from 'react-reveal/Slide';
// import styled, { css } from 'styled-components'

// class Carousel extends React.Component{

// render (){
//     const Container = styled.div`
//         // border: 1px solid red;
//         position: relative;
//         overflow: hidden;
//         width: 89rem;
//         height: 30rem;
// `;

// const Arrow = styled.div`
// //   text-shadow: 1px 1px 1px #fff;
//   z-index: 100;
//   line-height: 30rem;
//   text-align: center;
//   position: absolute;
//   top: 0;
//   width: 5%;
//   font-size: 2em;
//   cursor: pointer;
//   user-select: none;
//   ${props => props.right ? css`left: 94%;` : css`left: 0%;`}
// `;

// const CarouselUI = ({ position, handleClick,children }) => (
//     <Container>
//         {children}
//         <Arrow onClick={handleClick} data-position={position - 1}>{'<'}</Arrow>
//         <Arrow right onClick={handleClick} data-position={position + 1}>{'>'}</Arrow>
//     </Container>

// );
// const Carousel = makeCarousel(CarouselUI);
//     return(
//   <Carousel defaultWait={2000} maxTurns={2} /*wait for 1000 milliseconds*/ >
//     <Slide right>
//       <div >
//         {/* <img style={{width: '100%'}} src= "https://smppharmacy.com/wp-content/uploads/2019/02/food-post.jpg"/> */}
//         <img style={{width: '100%'}} src= "https://mindfulstudiomag.com/wp-content/uploads/2017/03/shutterstock_519988807-1500x600.jpg"/>
//       </div>
//     </Slide>
//     <Slide right>
//     <div >
//         <img style={{width: '100%'}} src= "https://mindfulstudiomag.com/wp-content/uploads/2018/05/shutterstock_575972317-1500x600.png"/>
//       </div>
//     </Slide>
//   </Carousel>
// );
// }
// }
// export default Carousel;

import React from 'react';
import makeCarousel from 'react-reveal/makeCarousel'
import Slide from 'react-reveal/Slide';
import styled from 'styled-components'

class Carousel extends React.Component {

  render() {

    const CarouselUI = ({ children }) => (<Container>{children}</Container>);

    const Container = styled.div`
    // border: 1px solid red;
    position: relative;
    overflow: hidden;
    width: 89rem;
    height: 30rem;
`;

    const Carousel = makeCarousel(CarouselUI);
    return (
      <Carousel defaultWait={2000} maxTurns={2} /*wait for 1000 milliseconds*/ >
        <Slide right>
          <div >
            {/* <img style={{width: '100%'}} src= "https://smppharmacy.com/wp-content/uploads/2019/02/food-post.jpg"/> */}
            <img style={{ width: '100%' }} src="https://mindfulstudiomag.com/wp-content/uploads/2017/03/shutterstock_519988807-1500x600.jpg" alt='' />
          </div>
        </Slide>
      </Carousel>

    );
  }
}
export default Carousel;