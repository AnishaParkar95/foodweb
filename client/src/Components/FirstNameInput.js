import React from 'react';
import TextField from '@material-ui/core/TextField';

class FirstNameInput extends React.Component {
    render() {

        const { FirstName, onFirstNameChange } = this.props;

        return (

            <div>
                <TextField
                    autoComplete="fname"
                    name="firstName"
                    variant="outlined"
                    value={FirstName}
                    onChange={onFirstNameChange}
                    required
                    fullWidth
                    id="firstName"
                    label="First Name"
                    autoFocus
                />
            </div>
        )
    }
}

export default FirstNameInput;