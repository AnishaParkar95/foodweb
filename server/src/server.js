const Express = require('express');
const Session = require('express-session');
const Mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(Session);
const User = require('./models/user');
const bcrypt = require('bcrypt');
const Recipe = require('./models/recipe');

const Two_Hours = 1000 * 60 * 60 * 2;

const {
    session_name = 'Session_id',
    session_lifetime = Two_Hours,
    node_env = 'DEVELOPMENT',
    session_secret = 'Shh,ThisisaSECRET',

} = process.env

const In_Production = node_env === 'PRODUCTION'

const app = Express();

Mongoose.connect('mongodb://localhost/foodwebdb', { useNewUrlParser: true })
Mongoose.connection.once('open', () => console.log("Connected to Database"))

//creating Session middleware 
app.use(Session({
    name: session_name,
    secret: session_secret,
    resave: false,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: Mongoose.connection }),
    cookie: {
        maxAge: session_lifetime,
        secure: In_Production,
        sameSite: true,
    }
}))

app.use(Express.json());

// When a GET request comes in on this route, find all users in the database and return them with a 200 code
app.get('/api/Users', async (request, response) => {

    console.log('A GET request came in asking for all users');

    const users = await User.find({});

    return response.send(users).status(200);
});

// When a POST request comes in on this route, create a new user and return success with a 200 code or failure with a 400 code
app.post('/api/users', async (request, response) => {

    console.log('A request came in with the body: ' + JSON.stringify(request.body));

    const { name, lname, emailAddress, password } = request.body;

    try {

        const existingUser = await User.findOne({ emailAddress: { $eq: emailAddress } });

        if (existingUser) {

            console.log(`A user with the email address '${emailAddress}' already exists, rejecting request with a 400`);

            return response.sendStatus(400);
        }

        bcrypt.hash(password, 12, async (error, hash) => {

            // Something went wrong (not common), return a 400
            if (error) {

                console.log('An error occured hashing the password: ' + error.message);

                return response.sendStatus(400);
            }

            // Create a new user with the password hash and email address
            const user = await User.create({ emailAddress, passwordDigest: hash, name, lname, });

            console.log('Saved user email was ' + user.emailAddress + ' and passwordDigest was ' + user.passwordDigest);

            return response.sendStatus(200);
        });
    } catch (error) {

        console.log('An unexpected error occured: ' + error.message);

        return response.sendStatus(500);
    }
});

app.post('/api/login', async (request, response) => {

    console.log(request.session);
    // Pull the login credentials from the request
    const { emailAddress, password } = request.body;

    // Find a user with the email address specified
    const user = await User.findOne({ emailAddress: { $eq: emailAddress } });

    // No user was found with that email address, dont tell the user this to mitigate user enumeration attacks, just return a 400
    if (!user) {

        console.log('No user was found with the email address: ' + emailAddress);

        return response.sendStatus(400);
    }

    // Let Bcrypt figure out if the given plaintext password equals the one saved in the database
    // Bcrypt will handle the random salt it gave the password when the user was created
    bcrypt.compare(password, user.passwordDigest, (error, result) => {

        // Not common, return a 400 if there was an error
        if (error) {

            console.error('There was an error checking the users password hash: ' + error.message);

            return response.sendStatus(400);
        }

        // Don't perform truthy logic here, check strict against a boolean
        if (result === true) {

            console.log('User successfully logged in!');
            request.session.userID = user.id;
            console.log('Session id is :', request.session.userID);
            return response.sendStatus(200);
        }

        console.log('User failed login, incorrect password');

        // Dont tell the user why the login failed, it just failed with a 400 ‾\_(ツ)_/‾
        return response.sendStatus(400);
    });
});

// When a POST request comes in on this route, create a new recipe 
app.post('/api/addrecipe', async (request, response) => {

    console.log('A request came in with the body: ' + JSON.stringify(request.body));

    const { recipetitle, description, ingredients, instructions } = request.body;

    // Create a new recipe
    const recipeadded = await Recipe.create({ recipetitle, description, ingredients, instructions });

    console.log('Saved recipe was ' + recipeadded.recipetitle +
        'and the description was' + recipeadded.description +
        ' and ingredients were ' + recipeadded.ingredients +
        'and instructions were' + recipeadded.instructions);

    return response.sendStatus(200);

});

app.get('/api/addrecipe', async (request, response) => {
    console.log('A GET request came in asking for all recipes');

    const allrecipes = await Recipe.find({});

    return response.send(allrecipes).status(200);

});

app.put('/api/users/_id', async (request, response) => {

    console.log('A request came in with the body: ' + JSON.stringify(request.body));

    const { name, lname, emailAddress, password } = request.body;

    console.log('A PUt request came in asking to update the user')

    try {
        const updateuser = await User.findById(request.params.id).exec();
        console.log('updates user is:', updateuser)
        updateuser.set(request.body)
        var result = await updateuser.save();
        response.send(result);

    } catch (error) {

        console.log('An unexpected error occured: ' + error.message);

        return response.sendStatus(500);
    }

});

const port = 4000;
app.listen(port, () => console.log(`Server has started on localhost:${port}`))