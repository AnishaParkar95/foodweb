const Mongoose = require ('mongoose')

const userSchema= new Mongoose.Schema({
        name:{type:String},
        lname:{type:String},
        emailAddress:{type:String},
        passwordDigest:{type:String}
});

const User = Mongoose.model('User', userSchema)

module.exports = User;