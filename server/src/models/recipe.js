const Mongoose = require ('mongoose')

const recipeSchema = new Mongoose.Schema({
    recipetitle:{type:String},
    description:{type:String},
    ingredients:{type:String},
    instructions:{type:String},

});

const Recipe = Mongoose.model('Recipe', recipeSchema)

module.exports = Recipe;